﻿using DemoApp.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Net.Http;

using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace DemoApp
{
	public partial class ProductService
    {
        HttpClient client;

        public List<Product> Items { get; private set; }
        private string RestUrl = @"https://makeup-api.herokuapp.com/api/v1/products.json?product_category=cream&product_type=eyeliner";

        public ProductService()
        {
            client = new HttpClient();
            client.MaxResponseContentBufferSize = 256000;
        }

        public async Task<List<Product>> GetProducts()
        {
            System.Diagnostics.Debug.WriteLine(RestUrl);
            Items = new List<Product>();
       
            var uri = new Uri(RestUrl);
           
            try
            {
                var response = await client.GetAsync(uri);
                if (response.IsSuccessStatusCode)
                {
                    var content = await response.Content.ReadAsStringAsync();
                    Items = JsonConvert.DeserializeObject<List<Product>>(content);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(@"				ERROR {0}", ex.Message);
            }
            foreach (Product pr in Items)
            {
                App.Database.SaveItem(pr);
            }
            //var new_items = App.Database.GetAll();
            return Items;
        }
    }
}