﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SQLite;

using Xamarin.Forms;

namespace DemoApp.Models
{
    [Table("Products")]
    public class Product
	{
        [PrimaryKey, AutoIncrement, Column("_id")]
        public int Id { get; set; }
        public string Brand { get; set; }
        public string Name { get; set; }
        public double Price { get; set; }
    }
}