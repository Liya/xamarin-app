﻿using Xamarin.Forms;

namespace DemoApp
{
	public class StartPage : ContentPage
	{
		public StartPage ()
		{
            Label header = new Label() { Text = "Привет из Xamarin Forms" };
            this.Content = header;
        }
	}
}